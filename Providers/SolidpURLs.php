<?php

namespace Modules\User\Providers;

use Illuminate\Support\Facades\Facade;

class SolidpURLs extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'solidp_url_generator';
    }
}