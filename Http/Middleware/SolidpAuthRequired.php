<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;

class SolidpAuthRequired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // (This middleware is just an interface to use on routes,
        // auth mechanism by itself is handle by JWTLogin middleware)
        return $next($request);
    }
}
