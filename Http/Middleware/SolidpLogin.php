<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Cookie;
use Modules\User\Entities\User;
use Modules\User\Providers\SolidpURLs;
use JWTAuth;
use Tymon\JWTAuth\Token;
use Tymon\JWTAuth\PayloadException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class SolidpLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // is authentification required
        $authRequired = in_array('solidp.auth.required', $request->route()->middleware());
        // route to reach on auth required
        $routeOnAuthRequired = config('user.RouteOnLoginRequest');

        // if user is not log in
        if(Auth::check() === false) {
            // try authenticate him from token
            try {
                // from GET params ( login attempt )
                if($token = $request->get('jwttoken')) {
                    $this->authenticateFromToken($token);
                    Cookie::queue('jwttoken', $token, config('jwt.ttl'));
                    return redirect(substr($request->requestUri, strpos($request->requestUri, '?jwttoken'))); //reload and remove the ?jwttoken from the url
                }
                // from COOKIE
                else if($token = $request->cookie('jwttoken')) {
                    $this->authenticateFromToken($token);
                    return $next($request);
                }
                // if no token
                else {
                    // seek auth if required
                    if($authRequired) {
                        //
                        if($routeOnAuthRequired) {
                            return redirect(route($routeOnAuthRequired));
                        }
                        else {
                            return $this->seekAuthentification($request);
                        }
                    }
                    // else just continue request
                    else {
                        return $next($request);
                    }
                }
            // if token is expired, try revalidation
            } catch (TokenExpiredException $e) {
                return $this->seekRevalidation($request);

            } catch (TokenInvalidException $e) {
                return $this->seekAuthentification($request);
            }
        }
        else {
            return $next($request);
        }

    }

    private function seekAuthentification($request)
    {
        $url = SolidpURLs::loginToUrl($request->fullUrl());
        return redirect($url);
    }

    private function seekRevalidation($request)
    {
        $url = SolidpURLs::revalidateToUrl($request->fullUrl());
        return redirect($url);
    }

    public function authenticateFromToken($rawToken)
    {
        $token = new Token($rawToken);
        $payload = JWTAuth::decode($token);

        // find user
        $user = User::where('email', $payload['email'])->first();

        // if not exist register him
        if(null === $user) {
            $user = User::create([
                'name' => $payload['prenom'].' '.$payload['nom'],
                'email' => $payload['email'],
                'password' => '',
                'nom' => $payload['nom'],
                'prenom' => $payload['prenom'],
                'pacage' => $payload['pacage'],
                'ville' => $payload['ville'],
                'CP' => $payload['CP']
            ]);
        }
        // else, update him
        else {
            $user->nom = $payload['nom'];
            $user->prenom = $payload['prenom'];
            $user->pacage = $payload['pacage'];
            $user->ville = $payload['ville'];
            $user->CP = $payload['CP'];
            $user->save();
        }

        // auth him
        Auth::loginUsingId($user->id);

    }
}
