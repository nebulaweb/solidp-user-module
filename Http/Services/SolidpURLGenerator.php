<?php

namespace Modules\User\Http\Services;

class SolidpURLGenerator
{

    protected $domain;
    protected $scheme;

    public function __construct() {

        $this->scheme = env('SOLIDP_SCHEME') ? env('SOLIDP_SCHEME') : config('user.solidp_scheme');
        $this->domain = env('SOLIDP_DOMAIN') ? env('SOLIDP_DOMAIN') : config('user.solidp_domain');
    }

    public function loginToRoute($route) {

        return $this->scheme.'://'.$this->domain.'/login?redirect='.urlencode(route($route));
    }

    public function loginToUrl($url) {

        return $this->scheme.'://'.$this->domain.'/login?redirect='.urlencode($url);
    }

    public function revalidateToRoute($route) {

        return $this->scheme.'://'.$this->domain.'/revalidate?redirect='.urlencode(route($route));
    }

    public function revalidateToUrl($url) {

        return $this->scheme.'://'.$this->domain.'/revalidate?redirect='.urlencode($url);
    }

    public function revalidateWithExpiredToken($route) {
        // to be tested
        return $this->scheme.'://'.$this->domain.'/revalidate?redirect='.urlencode(route($route)).'&jwttoken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9pZHAubG9jYWxcL2xvZ2luUG9zdCIsImlhdCI6MTU0OTUzMTk3MCwiZXhwIjoxNTQ5NTMyMDkwLCJuYmYiOjE1NDk1MzE5NzAsImp0aSI6InhERDhYT3hscGZhWWg5QXoiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJ1c2VybmFtZSI6InRlc3RtYW4iLCJlbWFpbCI6InRlc3RAbWFpbC5jb20ifQ.KOSP6-xpDbD_A4FRJB6AGg1lAa3m4xOpWd4B8Q4X4wA';
    }

    public function revalidateWithUnexpirableToken($route) {
        // to be tested
        return $this->scheme.'://'.$this->domain.'/revalidate?redirect='.urlencode(route($route)).'&jwttoken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9pZHAubG9jYWxcL2xvZ2luUG9zdCIsImlhdCI6MTU0OTUzNzM3NCwiZXhwIjoxOTA5NTM3Mzc0LCJuYmYiOjE1NDk1MzczNzQsImp0aSI6ImVsMTZkejAySFZvMVdSWk8iLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJ1c2VybmFtZSI6InRlc3RtYW4iLCJlbWFpbCI6InRlc3RAbWFpbC5jb20ifQ.Rbg97O9Gs61GSyA8B54CnHk0EV1gLogc9I39J2qEa2A';
    }
}
