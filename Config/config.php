<?php

return [
    'name' => 'User',
    'solidp_scheme' => 'http', // or set env('SOLIDP_SCHEME')
    'solidp_domain' => 'solidp.local', // or set env('SOLIDP_DOMAIN')
    'RouteOnLoginRequest' => 'solidp_login_request', //route to reach when authentification is needed (set to null for auto-redirection to solidp )
];
