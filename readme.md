## Installation

Start by replacing the User model in config/auth.php,
```
'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => Modules\User\Entities\User::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],
```


Then, run the migration to add new column to `users` table
```
php artisan migrate:refresh
```

Then, all you have to do is add middlewares to your routes.
Wrap all the restricted routes by `solidp.auth.required`, any non authenticated user accessing this routes will be redirected to authentification mechanism
```
Route::group(['middleware' => ['solidp.auth.enabled']], function() {

  Route::get('/', 'ExampleController@example')->name('example');
  Route::get('/logout', 'UserController@logout')->name('logout');
  //... others non-restricted routes

  Route::group(['middleware' => ['solidp.auth.required']], function() {

    //... your restricted routes

  });
});
```
Now, any requests to restricted routes will trigger an login attempt to Solidp Identity Provider

