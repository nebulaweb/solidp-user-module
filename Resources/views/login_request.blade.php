@extends('base')

@section('body')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Connexion à {{ env('APP_NAME') }}</h1>

            <div class="account-wall" style="text-align: center;">

                <img class="profile-img text-center" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">

                <br>
                 <a href="{{ \Modules\User\Providers\SolidpURLs::loginToRoute('home') }}" class="btn-solidp-primary">
                    <span>Connexion</span>
                    <strong>via<br> Sol-IDP</strong>
                </a>
            </div>
        </div>
    </div>
@endsection
